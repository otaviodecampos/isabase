package com.isabase;

import com.isabase.core.ApplicationManager;

import static spark.Spark.port;

/**
 * Created by otavio on 14/11/2015.
 */
public class Server {

    private static final int PORT = 9090;

    public static void main(String[] args) {
        port(PORT);
        ApplicationManager.boot();
    }

}

