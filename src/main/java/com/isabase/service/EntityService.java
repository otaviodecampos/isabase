package com.isabase.service;

import com.isabase.core.entity.BaseEntity;
import com.isabase.repository.EntityRepository;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by otavio on 21/11/2015.
 */
public class EntityService {

    @Inject
    private EntityRepository repository;

    public List<BaseEntity> findAll(String name) throws ClassNotFoundException {
        return repository.findAll(name);
    }

    public BaseEntity findById(String name, Integer pk) throws ClassNotFoundException {
        return repository.findById(name, pk);
    }

    public <T> BaseEntity findUniqueByField(String name, String fieldName, T fieldValue) throws ClassNotFoundException {
        return repository.findUniqueByField(name, fieldName, fieldValue);
    }

    public BaseEntity save(BaseEntity object) {
        return repository.save(object);
    }

    public void remove(BaseEntity o) {
        repository.remove(o);
    }

}
