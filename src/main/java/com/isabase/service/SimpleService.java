package com.isabase.service;

import org.apache.deltaspike.data.api.EntityRepository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by otavio on 15/11/2015.
 */
public abstract class SimpleService<Type, PK extends Serializable> {

    abstract EntityRepository<Type, PK> getRepository();

    public List<Type> findAll() {
        return getRepository().findAll();
    }

    public Type findById(PK pk) {
        return getRepository().findBy(pk);
    }

    public Type save(Type instance) {
        return getRepository().save(instance);
    }

    public void remove(Type instance) {
        getRepository().remove(instance);
    }

}