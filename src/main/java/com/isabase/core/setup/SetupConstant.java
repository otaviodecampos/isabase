package com.isabase.core.setup;

/**
 * Created by otavio on 24/12/2015.
 */
public class SetupConstant {

    public static final String SETUP_RUNNING_LOG = "Running setups";

    public static final String SETUP_FINISH_LOG = "The setup {0} is finished.";

    public static final String SETUP_FAILED_LOG = "The setup {0} failed.";

    public static final String SETUP_DISABLED_LOG = "The setup {0} is disabled.";

    public static final String SETUP_MANAGED_EXECUTED_LOG = "The managed setup {0} has already been run.";

    public static final int FIRST = 0;

    public static final int LAST = 10000;
}

