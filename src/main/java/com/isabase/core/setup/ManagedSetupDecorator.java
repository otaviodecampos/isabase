package com.isabase.core.setup;

/**
 * Created by otavio on 16/01/2016.
 */

import com.isabase.core.annotation.ManagedSetup;
import com.isabase.core.entity.ManagedSetupEntity;
import com.isabase.repository.ManagedSetupRepository;
import lombok.extern.java.Log;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;
import java.util.logging.Level;

@Log
@Decorator
public abstract class ManagedSetupDecorator implements Setup {

    @Inject
    @Delegate
    @ManagedSetup
    private Setup setup;

    @Inject
    private ManagedSetupRepository repository;

    public void setup() {
        String name = setup.getName();
        ManagedSetupEntity managedSetup = repository.findByNameEqual(name);

        if (managedSetup == null) {
            managedSetup = new ManagedSetupEntity(name);
        }

        if (!managedSetup.isExecuted()) {
            setup.setup();
            managedSetup.setExecuted(true);
            repository.save(managedSetup);
        } else {
            log.log(Level.INFO, SetupConstant.SETUP_MANAGED_EXECUTED_LOG, setup.getPath());
        }
    }

}
