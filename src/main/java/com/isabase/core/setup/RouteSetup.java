package com.isabase.core.setup;

import com.isabase.core.annotation.Order;
import com.isabase.route.Route;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

/**
 * Created by otavio on 11/01/2016.
 */
@Order(SetupConstant.LAST)
public class RouteSetup implements Setup {

    @Any
    @Inject
    private Instance<Route> routes;

    @Override
    public boolean run() {
        return true;
    }

    @Override
    public void setup() {
        for (Route route : routes) {
            route.findAll();
            route.findById();
            route.save();
            route.remove();
            route.onException();
        }
    }

}
