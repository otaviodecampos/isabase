package com.isabase.core.setup;

import com.isabase.core.annotation.ManagedSetup;
import com.isabase.core.annotation.Order;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.picketlink.annotations.PicketLink;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.model.basic.User;

import javax.inject.Inject;

import static org.picketlink.idm.model.basic.BasicModel.*;

/**
 * Created by otavio on 11/01/2016.
 */
@Order(SetupConstant.FIRST)
@ManagedSetup
@Transactional(qualifier = PicketLink.class)
public class SecuritySetup implements Setup {

    @Inject
    private PartitionManager partitionManager;

    @Inject
    private IdentityManager identityManager;

    @Override
    public boolean run() {
        return true;
    }

    @Override
    public void setup() {
        User admin = new User("admin");
        admin.setEmail("admin@appname");

        identityManager.add(admin);
        identityManager.updateCredential(admin, new Password("admin"));

        User otavio = new User("otaviodecampos");
        admin.setEmail("otaviodecampos@appname");

        identityManager.add(otavio);
        identityManager.updateCredential(otavio, new Password("123"));

        Role manager = new Role("manager"); // Create role "manager"
        identityManager.add(manager);

        Role superuser = new Role("superuser"); // Create application role "superuser"
        identityManager.add(superuser);

        Group adminGroup = new Group("admin"); // Create group "sales"
        identityManager.add(adminGroup);

        RelationshipManager relationshipManager = this.partitionManager.createRelationshipManager();
        addToGroup(relationshipManager, admin, adminGroup); // Make admin a member of the "admin" group
        addToGroup(relationshipManager, otavio, adminGroup); // Make admin a member of the "admin" group
        grantGroupRole(relationshipManager, admin, manager, adminGroup); // Make admin a manager of the "admin" group
        grantRole(relationshipManager, adminGroup, superuser); // Grant the "superuser" application role to "admin" group
    }
}
