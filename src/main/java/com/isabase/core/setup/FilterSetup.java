package com.isabase.core.setup;

import com.isabase.core.annotation.Order;
import com.isabase.filter.Filter;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import static spark.Spark.after;
import static spark.Spark.before;

/**
 * Created by otavio on 11/01/2016.
 */
@Order(SetupConstant.LAST)
public class FilterSetup implements Setup {

    @Any
    @Inject
    private Instance<Filter> filters;

    @Override
    public boolean run() {
        return true;
    }

    @Override
    public void setup() {
        for (Filter filter : filters) {
            before((request, response) -> filter.before(request, response));
            after((request, response) -> filter.after(request, response));
        }
    }

}
