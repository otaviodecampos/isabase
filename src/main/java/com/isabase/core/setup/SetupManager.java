package com.isabase.core.setup;

import com.isabase.core.annotation.Disable;
import com.isabase.core.annotation.ManagedSetup;
import com.isabase.core.annotation.Order;
import com.isabase.core.annotation.Startup;
import com.google.common.collect.Lists;
import lombok.extern.java.Log;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by otavio on 11/01/2016.
 */
@Log
@Startup
public class SetupManager {

    @Inject
    public SetupManager(@Any Instance<Setup> setups) throws RuntimeException {
        log.log(Level.INFO, SetupConstant.SETUP_RUNNING_LOG);
        for (Setup setup : orderSetups(setups)) {
            String path = setup.getPath();
            if (isEnabled(setup)) {
                try {
                    setup.setup();
                    log.log(Level.INFO, SetupConstant.SETUP_FINISH_LOG, path);
                } catch (Exception e) {
                    log.log(Level.SEVERE, SetupConstant.SETUP_FAILED_LOG, path);
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            } else {
                log.log(Level.INFO, SetupConstant.SETUP_DISABLED_LOG, path);
            }
        }
    }

    private boolean isEnabled(Setup setup) {
        Disable disable = setup.getClass().getAnnotation(Disable.class);
        if (disable != null) {
            return false;
        }
        return setup.run();
    }

    private boolean isManagedSetup(Setup setup) {
        ManagedSetup managedSetup = setup.getClass().getAnnotation(ManagedSetup.class);
        if (managedSetup != null) {
            return true;
        }
        return false;
    }

    private List<Setup> orderSetups(Instance<Setup> setups) {
        List<Setup> ordered = Lists.newArrayList(setups);
        ordered.sort(new Comparator<Setup>() {

            private int getOrder(Setup setup) {
                Order order = setup.getClass().getAnnotation(Order.class);
                if (order != null) {
                    return order.value();
                }
                return 0;
            }

            @Override
            public int compare(Setup setup, Setup setup2) {
                return getOrder(setup) - getOrder(setup2);
            }
        });

        return ordered;
    }

}