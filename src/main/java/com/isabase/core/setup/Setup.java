package com.isabase.core.setup;

import com.isabase.util.ClassUtil;

/**
 * Created by otavio on 11/01/2016.
 */
public interface Setup {

    boolean run();

    void setup();

    default String getName() {
        return ClassUtil.getName(this.getClass());
    }

    default String getPath() {
        return ClassUtil.getPath(this.getClass());
    }

}
