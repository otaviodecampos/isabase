package com.isabase.core.persistence.listener;

import com.isabase.core.ApplicationManager;
import com.isabase.core.annotation.Proxy;
import com.isabase.core.entity.model.EntityModel;
import com.isabase.core.persistence.jpa.EntityManagerFactoryProxy;
import com.isabase.security.CurrentUser;

import javax.enterprise.util.AnnotationLiteral;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;

/**
 * Created by otavio on 22/11/2015.
 */
public class EntityModelListener {

    @PostUpdate
    @PostPersist
    @PostRemove
    private void postChanging(EntityModel entityModel) {
        EntityManagerFactoryProxy proxy = ApplicationManager.getBean(EntityManagerFactoryProxy.class, new AnnotationLiteral<Proxy>() {});
        proxy.scheduleUpdate();
    }

    @PrePersist
    private void preChanging(EntityModel entityModel) {
        CurrentUser user = ApplicationManager.getBean(CurrentUser.class);
        if(user.isLoggedIn()) {
            entityModel.setOwner(user.getLoginName());
        }
    }

}
