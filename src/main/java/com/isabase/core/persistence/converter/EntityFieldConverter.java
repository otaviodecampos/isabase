package com.isabase.core.persistence.converter;

import com.isabase.core.entity.model.EntityField;
import com.isabase.util.JsonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 21/11/2015.
 */
@Converter
public class EntityFieldConverter implements AttributeConverter<List<EntityField>, String> {

    @Override
    public String convertToDatabaseColumn(List<EntityField> entityFieldModels) {
        try {
            return JsonUtil.toJson(entityFieldModels, new TypeReference<List<EntityField>>() {} );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public List<EntityField> convertToEntityAttribute(String s) {
        try {
            return JsonUtil.fromJson(s, new TypeReference<List<EntityField>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
