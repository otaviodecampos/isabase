package com.isabase.core.persistence.jpa;

import com.isabase.core.ApplicationManager;
import com.isabase.core.annotation.Proxy;
import com.isabase.core.annotation.Startup;
import com.isabase.core.dynamic.DynamicManager;
import com.isabase.security.CurrentUser;
import lombok.extern.java.Log;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by otavio on 23/11/2015.
 */
@Log
@Proxy
@ApplicationScoped
public class EntityManagerFactoryProxy implements EntityManagerFactory {

    @Inject
    private DynamicManager dm;

    @Inject
    private CurrentUser user;

    private Map<String, EntityManagerFactory> tenantEMFactory = new HashMap();

    private Map<String, Boolean> tenantUpdate = new HashMap();

    private Map<String, Thread> tenantUpdater = new HashMap();

    @Startup
    @Produces
    private EntityManagerFactory init() {
        return getDelegate();
    }

    public EntityManagerFactory getDelegate() {
        return getDelegate(user.getLoginName());
    }

    public EntityManagerFactory getDelegate(String loginName) {
        EntityManagerFactory delegate = tenantEMFactory.get(loginName);
        if (delegate == null || !delegate.isOpen()) {
            delegate = create(dm.getEntityManagerFactoryProperties(true));
            tenantEMFactory.put(loginName, delegate);
            log.log(Level.INFO, PersistenceConstant.FACTORY_PROXY_DELEGATE, loginName);
        }
        return delegate;
    }

    public void update() {
        update(user.getLoginName());
    }

    public void update(String loginName) {
        if (tenantUpdate.get(loginName) != null && tenantUpdate.get(loginName)) {
            tenantUpdate.put(loginName, false);
            getDelegate(loginName).close();
            getDelegate(loginName);
        }
    }

    public EntityManagerFactory create() {
        return create(new HashMap());
    }

    public EntityManagerFactory create(Map<String, java.lang.Object> properties) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(PersistenceConstant.PERSISTENCE_UNIT_NAME, properties);
        log.log(Level.INFO, PersistenceConstant.FACTORY_PROXY_NEW, getCurrentTenantIdentifier());
        return emf;
    }

    public void scheduleUpdate() {
        tenantUpdate.put(user.getLoginName(), true);
    }

    private void waitUpdate() {
        if (tenantUpdater.get(user.getLoginName()) != null && tenantUpdater.get(user.getLoginName()).isAlive()) {
            try {
                tenantUpdater.get(user.getLoginName()).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void close() {
        String loginName = user.getLoginName();
        Thread updater = new Thread(() -> {
            ApplicationManager.runInContext(RequestScoped.class, () -> update(loginName));
        });
        tenantUpdater.put(user.getLoginName(), updater);
        updater.start();
    }

    @Override
    public EntityManager createEntityManager() {
        waitUpdate();
        return getDelegate().createEntityManager();
    }

    @Override
    public EntityManager createEntityManager(Map map) {
        waitUpdate();
        return getDelegate().createEntityManager(map);
    }

    @Override
    public EntityManager createEntityManager(SynchronizationType synchronizationType) {
        waitUpdate();
        return getDelegate().createEntityManager(synchronizationType);
    }

    @Override
    public EntityManager createEntityManager(SynchronizationType synchronizationType, Map map) {
        waitUpdate();
        return getDelegate().createEntityManager(synchronizationType, map);
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return getDelegate().getCriteriaBuilder();
    }

    @Override
    public Metamodel getMetamodel() {
        return getDelegate().getMetamodel();
    }

    @Override
    public boolean isOpen() {
        return getDelegate().isOpen();
    }

    @Override
    public Map<String, Object> getProperties() {
        return getDelegate().getProperties();
    }

    @Override
    public Cache getCache() {
        return getDelegate().getCache();
    }

    @Override
    public PersistenceUnitUtil getPersistenceUnitUtil() {
        return getDelegate().getPersistenceUnitUtil();
    }

    @Override
    public void addNamedQuery(String s, Query query) {
        getDelegate().addNamedQuery(s, query);
    }

    @Override
    public <T> T unwrap(Class<T> aClass) {
        return getDelegate().unwrap(aClass);
    }

    @Override
    public <T> void addNamedEntityGraph(String s, EntityGraph<T> entityGraph) {
        getDelegate().addNamedEntityGraph(s, entityGraph);
    }

    private String getCurrentTenantIdentifier() {
        return user.isLoggedIn() ? PersistenceConstant.PERSISTENCE_UNIT_NAME + "_" + user.getLoginName() : PersistenceConstant.PERSISTENCE_UNIT_NAME;
    }
}
