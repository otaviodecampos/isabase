package com.isabase.core.persistence.jpa;

import com.isabase.core.annotation.Proxy;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;

/**
 * Created by otavio on 16/11/2015.
 */
@ApplicationScoped
public class EntityManagerFactoryProducer {

    @Proxy
    @Inject
    private EntityManagerFactoryProxy proxy;

    @Produces
    @RequestScoped
    public EntityManagerFactory create() {
        return proxy;
    }

    public void destroy(@Disposes EntityManagerFactory factory) {
        factory.close();
    }

}
