package com.isabase.core.persistence.jpa;

import org.picketlink.annotations.PicketLink;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Created by otavio on 15/11/2015.
 */
@ApplicationScoped
public class EntityManagerProducer {

    @Inject
    private EntityManagerFactory emf;

    @Default
    @Produces
    @RequestScoped
    public EntityManager create() {
        EntityManager em = emf.createEntityManager();
        return em;
    }

    @Produces
    @PicketLink
    @RequestScoped
    public EntityManager createByPicketLink() {
        EntityManager em = emf.createEntityManager();
        return em;
    }

    public void close(@Disposes EntityManager em) {
        if (em.isOpen()) {
            em.close();
        }
    }

}
