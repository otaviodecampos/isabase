package com.isabase.core.persistence.jpa;

/**
 * Created by otavio on 17/11/2015.
 */
public class PersistenceConstant {

    public static final String PERSISTENCE_UNIT_NAME = "appname";

    public static final String PERSISTENCE_UNIT_TENANT_NAME = "appname_tenant";

    public static final String FACTORY_PROXY_DELEGATE = "New entity manager factory delegated to tenant: {0}";

    public static final String FACTORY_PROXY_NEW = "Created entity manager factory to tenant: {0}";

}
