package com.isabase.core;

import com.isabase.core.annotation.Startup;
import org.jboss.weld.environment.se.events.ContainerInitialized;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Singleton;
import java.util.Set;

/**
 * Created by otavio on 16/01/2016.
 */
@Singleton
public class StartupManager {

    public void init(@Observes ContainerInitialized event, BeanManager beanManager) {

        Set<Bean<?>> beans = beanManager.getBeans(Object.class, new AnnotationLiteral<Startup>() {});

        for (Bean<?> bean : beans) {
            ApplicationManager.runInContext(RequestScoped.class, () ->
                    beanManager.getReference(bean, bean.getTypes().iterator().next(), beanManager.createCreationalContext(bean)).toString());
        }

    }

}
