package com.isabase.core;

/**
 * Created by otavio on 16/01/2016.
 */
public interface CodeBlock {

    void run();

}
