package com.isabase.core.dynamic;

import com.isabase.core.ExceptionConstant;
import com.isabase.core.annotation.ClassBuilder;
import com.isabase.core.annotation.Proxy;
import com.isabase.core.dynamic.builder.DynamicClassBuilder;
import com.isabase.core.dynamic.exception.DynamicClassBuildException;
import com.isabase.core.entity.model.EntityField;
import com.isabase.core.entity.model.EntityModel;
import com.isabase.core.entity.model.FieldType;
import com.isabase.core.entity.model.QEntityModel;
import com.isabase.core.entity.model.field.ModelEntityField;
import com.isabase.core.persistence.jpa.EntityManagerFactoryProxy;
import com.isabase.security.CurrentUser;
import com.mysema.query.jpa.impl.JPAQuery;
import lombok.Data;
import lombok.extern.java.Log;
import org.hibernate.cfg.AvailableSettings;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.EntityType;
import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;

/**
 * Created by otavio on 21/11/2015.
 */
@Log
@Data
@ApplicationScoped
public class DynamicManager {

    @Proxy
    @Inject
    private EntityManagerFactoryProxy proxy;

    @Inject
    @ClassBuilder
    private DynamicClassBuilder dynamicClassBuilder;

    @Inject
    private CurrentUser user;

    private Map<String, Map<String, Class<?>>> classesByUser = new HashMap();

    private Map<String, Map<String, EntityModel>> modelsByUser = new HashMap();

    public Map<String, Object> getEntityManagerFactoryProperties(Boolean dynamic) {
        Map<String, Object> properties = new HashMap<>();
        if (dynamic) {
            properties.put(org.hibernate.jpa.AvailableSettings.LOADED_CLASSES, loadDynamicClasses());
            properties.put(AvailableSettings.CLASSLOADERS, Arrays.asList(dynamicClassBuilder.getClassLoader()));
        }
        return properties;
    }

    public void updateDynamicClasses() {
        proxy.update();
    }

    private Collection<Class<?>> loadDynamicClasses() {
        long startTime = System.currentTimeMillis();

        Map<String, Class<?>> classes = classesByUser.get(user.getLoginName());
        Map<String, EntityModel> models = modelsByUser.get(user.getLoginName());

        if(classes == null) {
            classes = new HashMap<String, Class<?>>();
            classesByUser.put(user.getLoginName(), classes);

            models = new HashMap<String, EntityModel>();
            modelsByUser.put(user.getLoginName(), models);
        }

        classes.clear();
        models.clear();

        dynamicClassBuilder.clear();

        EntityManagerFactory emf = proxy.create();
        EntityManager em = emf.createEntityManager();

        List<Class<?>> classList = new ArrayList<>();

        QEntityModel path = new QEntityModel("o");
        JPAQuery q = new JPAQuery(em).from(path);

        if (user.isLoggedIn()) {
            q = q.where(path.owner.eq(user.getLoginName()));
        }

        for (EntityModel model : q.list(path)) {
            log.log(Level.INFO, DynamicConstant.DYNAMIC_CLASS_LOAD_LOG, DynamicConstant.DYNAMIC_PACKAGE + model.getName());
            Class<?> clazz = null;
            try {
                clazz = dynamicClassBuilder.build(buildReferences(model));
            } catch (DynamicClassBuildException e) {
                log.log(Level.SEVERE, DynamicConstant.DYNAMIC_CLASS_BUILD_EXCEPTION, DynamicConstant.DYNAMIC_PACKAGE + model.getName());
                e.printStackTrace();
            }
            classList.add(clazz);
            classes.put(model.getName().toLowerCase(), clazz);
            models.put(model.getName().toLowerCase(), model);
        }

        for (EntityType entity : em.getMetamodel().getEntities()) {
            classList.add(entity.getJavaType());
            classes.put(entity.getName().toLowerCase(), entity.getJavaType());
            models.put(entity.getName().toLowerCase(), getModelByClass(entity.getJavaType()));
        }

        em.close();
        emf.close();

        long totalTime = System.currentTimeMillis() - startTime;
        log.log(Level.INFO, DynamicConstant.DYNAMIC_CLASSES_LOADED_LOG, totalTime);

        return classList;
    }

    public DynamicEntity newInstance(String modelName) throws ClassNotFoundException {
        Class<DynamicEntity> modelClass = getEntityClass(modelName);
        try {
            return modelClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private EntityModel buildReferences(EntityModel model) {
        model.getFields().stream()
                .filter(field -> field instanceof ModelEntityField)
                .map(field -> (ModelEntityField) field)
                .forEach(field -> {
                    if (classesByUser.get(user.getLoginName()).get(field.getTarget().toLowerCase()) == null) {
                        try {
                            dynamicClassBuilder.build(new EntityModel(field.getTarget()));
                        } catch (DynamicClassBuildException e) {
                            log.log(Level.SEVERE, DynamicConstant.DYNAMIC_CLASS_BUILD_EXCEPTION, DynamicConstant.DYNAMIC_PACKAGE + model.getName());
                            e.printStackTrace();
                        }
                    }
                });
        return model;
    }

    public Collection<EntityModel> getModels() {
        return modelsByUser.get(user.getLoginName()).values();
    }

    public EntityModel getModel(String name) {
        return modelsByUser.get(user.getLoginName()).get(name.toLowerCase());
    }

    public Class getEntityClass(String name) throws ClassNotFoundException {

        if(classesByUser.get(user.getLoginName()) == null) {
            proxy.getDelegate(name.toLowerCase());
        }

        Class clazz = classesByUser.get(user.getLoginName()).get(name.toLowerCase());
        if(clazz == null) {
            throw new ClassNotFoundException(ExceptionConstant.ENTITY_NOT_FOUND);
        }
        return clazz;
    }

    public String getEntityName(String name) {
        return classesByUser.get(user.getLoginName()).get(name.toLowerCase()).getSimpleName();
    }

    public EntityModel getModelByClass(Class clazz) {
        EntityModel model = new EntityModel();
        model.setName(clazz.getSimpleName());

        for (Field field : clazz.getDeclaredFields()) {
            EntityField entityField = FieldType.newInstanceByClass(field.getType());
            if (entityField != null) {
                entityField.setName(field.getName());
                model.getFields().add(entityField);
            }
        }

        return model;
    }

}
