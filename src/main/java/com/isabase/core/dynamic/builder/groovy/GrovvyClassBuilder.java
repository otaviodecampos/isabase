package com.isabase.core.dynamic.builder.groovy;

import com.isabase.core.annotation.ClassBuilder;
import com.isabase.core.entity.model.EntityModel;
import com.isabase.core.dynamic.builder.DynamicClassBuilder;
import groovy.lang.GroovyClassLoader;

import javax.enterprise.context.ApplicationScoped;

/**
 * Created by otavio on 21/11/2015.
 */
@ApplicationScoped
@ClassBuilder
public class GrovvyClassBuilder implements DynamicClassBuilder {

    private GroovyClassLoader classLoader = new GroovyClassLoader();

    private GroovyClassLayout classLayout = new GroovyClassLayout();

    @Override
    public void clear() {}

    @Override
    public Class<?> build(EntityModel entityModel) {
        return classLoader.parseClass(classLayout.build(entityModel));
    }

    @Override
    public ClassLoader getClassLoader() {
        return classLoader;
    }
}
