package com.isabase.core.dynamic.builder.groovy.annotationfield.builder;

import com.isabase.core.dynamic.builder.groovy.annotationfield.ColumnAnnotationField;
import com.isabase.core.entity.model.field.DecimalEntityField;

/**
 * Created by otavio on 19/12/2015.
 */
public class DecimalAnnotationBuilder implements AnnotationBuilder<ColumnAnnotationField, DecimalEntityField> {

    @Override
    public ColumnAnnotationField build(DecimalEntityField entityField) {
        ColumnAnnotationField annotation = new ColumnAnnotationField();

        annotation.setPrecision(entityField.getPrecision());
        annotation.setScale(entityField.getScale());

        return annotation;
    }

}
