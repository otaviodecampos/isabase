package com.isabase.core.dynamic.builder;

import com.isabase.core.entity.model.EntityModel;
import com.isabase.core.dynamic.exception.DynamicClassBuildException;

/**
 * Created by otavio on 21/11/2015.
 */
public interface DynamicClassBuilder {

    void clear();

    Class<?> build(EntityModel model) throws DynamicClassBuildException;

    ClassLoader getClassLoader();

}
