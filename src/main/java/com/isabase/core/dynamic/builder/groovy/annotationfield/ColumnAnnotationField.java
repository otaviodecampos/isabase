package com.isabase.core.dynamic.builder.groovy.annotationfield;

import lombok.Data;

/**
 * Created by otavio on 18/12/2015.
 */
@Data
public class ColumnAnnotationField extends GroovyAnnotationField {

    private String name = "Column";

    private Integer length;

    private Integer precision;

    private Integer scale;

}
