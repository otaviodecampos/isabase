package com.isabase.core.dynamic.builder.groovy;

import com.isabase.core.dynamic.DynamicConstant;
import com.isabase.core.dynamic.builder.groovy.annotationfield.GroovyAnnotationField;
import com.isabase.core.dynamic.builder.groovy.annotationfield.builder.AnnotationBuilder;
import com.isabase.core.dynamic.builder.groovy.annotationfield.builder.DecimalAnnotationBuilder;
import com.isabase.core.dynamic.builder.groovy.annotationfield.builder.ModelAnnotationBuilder;
import com.isabase.core.dynamic.builder.groovy.annotationfield.builder.TextAnnotationBuilder;
import com.isabase.core.entity.model.EntityField;
import com.isabase.core.entity.model.field.DecimalEntityField;
import com.isabase.core.entity.model.field.ModelEntityField;
import com.isabase.core.entity.model.field.TextEntityField;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by otavio on 29/11/2015.
 */
public class GroovyClassUtil {

    private static Map<Class<? extends EntityField>, List<AnnotationBuilder>> typeAnnotation = new HashMap() {{
        put(TextEntityField.class, Arrays.asList(new TextAnnotationBuilder()));
        put(DecimalEntityField.class, Arrays.asList(new DecimalAnnotationBuilder()));
        put(ModelEntityField.class, Arrays.asList(new ModelAnnotationBuilder()));
    }};

    public static List<GroovyAnnotationField> getEntityFieldAnnotations(EntityField field) {

        List<GroovyAnnotationField> annotations = typeAnnotation.get(field.getClass())
                .stream()
                .map(builder -> builder.build(field))
                .collect(Collectors.toList());

        return annotations;
    }

    public static String typeByEntityField(EntityField field) {
        String type = field.getClassType().getSimpleName();
        if (field instanceof ModelEntityField) {
            ModelEntityField modelField = (ModelEntityField) field;
            type = DynamicConstant.DYNAMIC_PACKAGE + "." + modelField.getTarget();
            if (modelField.isCollection()) {
                type = "List<" + type + ">";
            }
        }
        return type;
    }

}
