package com.isabase.core.dynamic.builder.groovy.annotationfield;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;

/**
 * Created by otavio on 18/12/2015.
 */
@Data
public abstract class ModelAnnotationField extends GroovyAnnotationField {

    private CascadeType cascade = CascadeType.ALL;

    private FetchType fetch = FetchType.EAGER;

}
