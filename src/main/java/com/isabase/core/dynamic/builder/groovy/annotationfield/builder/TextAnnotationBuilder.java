package com.isabase.core.dynamic.builder.groovy.annotationfield.builder;

import com.isabase.core.dynamic.builder.groovy.annotationfield.ColumnAnnotationField;
import com.isabase.core.entity.model.field.TextEntityField;

/**
 * Created by otavio on 19/12/2015.
 */
public class TextAnnotationBuilder implements AnnotationBuilder<ColumnAnnotationField, TextEntityField> {

    @Override
    public ColumnAnnotationField build(TextEntityField entityField) {
        ColumnAnnotationField annotation = new ColumnAnnotationField();

        if (entityField.isTextarea()) {
            annotation.setLength(2147483647);
        }

        return annotation;
    }

}
