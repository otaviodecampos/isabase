package com.isabase.core.dynamic.builder.groovy.annotationfield;

import lombok.Data;

import javax.persistence.FetchType;

/**
 * Created by otavio on 18/12/2015.
 */
@Data
public class OneToOneAnnotationField extends ModelAnnotationField {

    private String name = "OneToOne";

    private FetchType fetch = FetchType.EAGER;

}
