package com.isabase.core.dynamic.builder.groovy.annotationfield;

/**
 * Created by otavio on 18/12/2015.
 */
public interface AnnotationField {

    String getName();

}
