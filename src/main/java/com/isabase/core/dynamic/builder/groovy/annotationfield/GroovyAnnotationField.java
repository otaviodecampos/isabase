package com.isabase.core.dynamic.builder.groovy.annotationfield;

import com.google.common.primitives.Primitives;
import org.apache.commons.lang.text.StrSubstitutor;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by otavio on 18/12/2015.
 */
public abstract class GroovyAnnotationField implements AnnotationField {

    private String template = "@${name}(${properties})";

    public String write() {
        Map<String, String> values = new HashMap<>();
        values.put("name", this.getName());
        values.put("properties", this.getProperties(this.getClass()).toString().replaceAll("\\{|\\}", ""));
        StrSubstitutor sub = new StrSubstitutor(values);
        return sub.replace(template);
    }

    private Map<String, String> getProperties(Class clazz) {
        Map<String, String> properties = new HashMap();

        for (Field field : clazz.getDeclaredFields()) {

            if (field.getName().equals("name")) {
                continue;
            }

            try {
                field.setAccessible(true);
                Object value = field.get(this);

                if (value == null) {
                    continue;
                }

                String strValue = null;
                if (Primitives.isWrapperType(value.getClass()) || value instanceof String) {
                    strValue = value.toString();
                } else {
                    strValue = value.getClass().getName() + "." + value.toString();
                }
                properties.put(field.getName(), strValue);

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } finally {
                field.setAccessible(false);
            }
        }

        Class superClazz = clazz.getSuperclass();
        if (superClazz != null && superClazz != GroovyAnnotationField.class) {
            properties.putAll(getProperties(superClazz));
        }

        return properties;
    }

}
