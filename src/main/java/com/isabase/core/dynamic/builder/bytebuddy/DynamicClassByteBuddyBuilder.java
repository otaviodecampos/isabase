package com.isabase.core.dynamic.builder.bytebuddy;

import com.isabase.core.dynamic.DynamicConstant;
import com.isabase.core.dynamic.DynamicEntity;
import com.isabase.core.dynamic.builder.DynamicClassBuilder;
import com.isabase.core.dynamic.builder.bytebuddy.annotationtype.EntityAnnotation;
import com.isabase.core.entity.model.EntityField;
import com.isabase.core.entity.model.EntityModel;
import com.isabase.util.StringUtil;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.modifier.Visibility;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.FieldAccessor;

import javax.enterprise.context.ApplicationScoped;
import java.util.Arrays;

/**
 * Created by otavio on 21/11/2015.
 */
@Deprecated
@ApplicationScoped
public class DynamicClassByteBuddyBuilder implements DynamicClassBuilder {

    private ClassLoader classLoader;

    @Override
    public void clear() {
        classLoader = getClass().getClassLoader();
    }

    @Override
    public Class<?> build(EntityModel model) {
        DynamicType.Builder builder = new ByteBuddy().subclass(DynamicEntity.class);
        builder = builder.name(getClassName(model));

        builder = builder.annotateType(new EntityAnnotation());

        for (EntityField field : model.getFields()) {
            String name = field.getName();
            Class typeClass = field.getClassType();

            builder = builder.defineField(name, typeClass, Visibility.PRIVATE)
                    .defineMethod(getSetterName(name), Void.TYPE, Arrays.asList(typeClass), Visibility.PUBLIC).intercept(FieldAccessor.ofField(name))
                    .defineMethod(getGetterName(name), typeClass, Arrays.asList(), Visibility.PUBLIC).intercept(FieldAccessor.ofField(name));
        }

        DynamicType.Unloaded unloadedType = builder.make();
        DynamicType.Loaded loadedType = unloadedType.load(classLoader, ClassLoadingStrategy.Default.WRAPPER);

        Class<?> clazz = loadedType.getLoaded();
        classLoader = clazz.getClassLoader();
        return clazz;
    }

    @Override
    public ClassLoader getClassLoader() {
        return classLoader;
    }

    private String getSetterName(String name) {
        return "set" + StringUtil.capitalize(name);
    }

    private String getGetterName(String name) {
        return "get" + StringUtil.capitalize(name);
    }

    private String getClassName(EntityModel entityModel) {
        return DynamicConstant.DYNAMIC_PACKAGE + "." + StringUtil.capitalize(entityModel.getName());
    }
}
