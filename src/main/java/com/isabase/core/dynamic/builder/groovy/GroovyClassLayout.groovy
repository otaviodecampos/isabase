package com.isabase.core.dynamic.builder.groovy

import com.isabase.core.dynamic.DynamicConstant
import com.isabase.core.entity.model.EntityModel
import com.isabase.util.StringUtil
import groovy.text.SimpleTemplateEngine
/**
 * Created by otavio on 21/11/2015.
 */
class GroovyClassLayout {

    private static SimpleTemplateEngine engine = new SimpleTemplateEngine();

    public String build(EntityModel model) {

        def tpl_src = '''
            package ${DynamicConstant.DYNAMIC_PACKAGE};

            import javax.persistence.*;
            import com.isabase.core.entity.model.Model;
            import com.isabase.core.dynamic.DynamicEntity;

            @Entity
            public class ${ model.name } extends DynamicEntity {
                <% for (field in model.fields) {
                    def name = field.name
                    def annotations = GroovyClassUtil.getEntityFieldAnnotations(field)
                    def type = GroovyClassUtil.typeByEntityField(field)

                    for (annotationfield in annotations) {
                        println annotationfield.write();
                    }
                    println "private ${type} ${name}"
                    println ""
                    println "public ${type} get${StringUtil.capitalize(name)}() { return ${name}; }"
                    println ""
                    println "public void set${StringUtil.capitalize(name)}(${type} value) { this.${name} = value; }"
                }%>
            }

		'''

        def binding = ["model": model,
                       "StringUtil": StringUtil,
                       "GroovyClassUtil": GroovyClassUtil,
                       "DynamicConstant": DynamicConstant]
        def template = engine.createTemplate(tpl_src).make(binding)
        return template.toString();
    }
}
