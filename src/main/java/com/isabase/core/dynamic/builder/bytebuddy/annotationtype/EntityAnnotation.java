package com.isabase.core.dynamic.builder.bytebuddy.annotationtype;

import javax.persistence.Entity;
import java.lang.annotation.Annotation;

/**
 * Created by otavio on 18/11/2015.
 */

public class EntityAnnotation implements Entity {

    public Class<? extends Annotation> annotationType() {
        return Entity.class;
    }

    @Override
    public String name() {
        return "";
    }
}