package com.isabase.core.dynamic.builder.groovy.annotationfield.builder;

import com.isabase.core.dynamic.builder.groovy.annotationfield.OneToManyAnnotationField;
import com.isabase.core.dynamic.builder.groovy.annotationfield.OneToOneAnnotationField;
import com.isabase.core.dynamic.builder.groovy.annotationfield.ModelAnnotationField;
import com.isabase.core.entity.model.field.ModelEntityField;

/**
 * Created by otavio on 19/12/2015.
 */
public class ModelAnnotationBuilder implements AnnotationBuilder<ModelAnnotationField, ModelEntityField> {

    @Override
    public ModelAnnotationField build(ModelEntityField entityField) {
        ModelAnnotationField annotation = null;

        if (entityField.isCollection()) {
            annotation = new OneToManyAnnotationField();
        } else {
            annotation = new OneToOneAnnotationField();
        }

        return annotation;
    }

}
