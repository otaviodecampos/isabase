package com.isabase.core.dynamic.builder.groovy.annotationfield.builder;

import com.isabase.core.dynamic.builder.groovy.annotationfield.GroovyAnnotationField;

/**
 * Created by otavio on 19/12/2015.
 */
public interface AnnotationBuilder<B extends GroovyAnnotationField, T> {

    B build(T entityField);

}
