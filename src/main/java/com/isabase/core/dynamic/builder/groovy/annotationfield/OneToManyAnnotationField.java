package com.isabase.core.dynamic.builder.groovy.annotationfield;

import lombok.Data;

import javax.persistence.FetchType;

/**
 * Created by otavio on 18/12/2015.
 */
@Data
public class OneToManyAnnotationField extends ModelAnnotationField {

    private String name = "OneToMany";

    private FetchType fetch = FetchType.LAZY;

}
