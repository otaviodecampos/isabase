package com.isabase.core.dynamic.util;

import com.isabase.core.entity.model.FieldType;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by otavio on 20/12/2015.
 */
public class DynamicUtil {

    public static ObjectMapper newObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        for (FieldType fieldType : FieldType.values()) {
            mapper.registerSubtypes(fieldType.getClazz());
        }
        return mapper;
    }

}
