package com.isabase.core.dynamic;

import com.isabase.core.dynamic.exception.DynamicInstanceFieldException;
import com.isabase.util.StringUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by otavio on 14/11/2015.
 */
public class DynamicEntityField<T> {

    private DynamicEntity instance;

    private Field field;

    private Method setter;

    private Method getter;

    public DynamicEntityField(DynamicEntity instance, String name) throws DynamicInstanceFieldException {
        this.instance = instance;
        Class clazz = this.instance.getClass();

        try {
            this.field = clazz.getDeclaredField(name);
            this.setter = clazz.getDeclaredMethod("set" + StringUtil.capitalize(name), instance.getModel().getField(name).getClassType());
            this.getter = clazz.getDeclaredMethod("get" + StringUtil.capitalize(name));
        } catch (NoSuchFieldException | NoSuchMethodException e) {
            throw new DynamicInstanceFieldException(DynamicConstant.INSTANCE_FIELD_ACCESS_EXCEPTION);
        }
    }

    public T getValue() throws DynamicInstanceFieldException {
        try {
            return (T) getter.invoke(instance);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new DynamicInstanceFieldException(DynamicConstant.INSTANCE_FIELD_ACCESS_EXCEPTION);
        }
    }

    public void setValue(T value) throws DynamicInstanceFieldException {
        try {
            setter.invoke(instance, value);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new DynamicInstanceFieldException(DynamicConstant.INSTANCE_FIELD_ACCESS_EXCEPTION);
        }
    }

}