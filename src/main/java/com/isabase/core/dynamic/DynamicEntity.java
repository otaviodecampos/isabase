package com.isabase.core.dynamic;

import com.isabase.core.entity.BaseEntity;
import com.isabase.core.entity.model.EntityModel;
import com.isabase.core.dynamic.exception.DynamicInstanceFieldException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.deltaspike.core.api.provider.BeanProvider;

import javax.persistence.MappedSuperclass;

/**
 * Created by otavio on 14/11/2015.
 */
@Data
@MappedSuperclass
public abstract class DynamicEntity extends BaseEntity<Integer> {

    public <ClassType> DynamicEntityField<ClassType> getField(String name) throws DynamicInstanceFieldException {
        return new DynamicEntityField<ClassType>(this, name);
    }

    @JsonIgnore
    public EntityModel getModel() {
        DynamicManager dm = BeanProvider.getContextualReference(DynamicManager.class);
        return dm.getModel(this.getClass().getSimpleName());
    }

}