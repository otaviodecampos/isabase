package com.isabase.core.dynamic.exception;

/**
 * Created by otavio on 24/12/2015.
 */
public class DynamicInstanceFieldException extends Exception {

    public DynamicInstanceFieldException() {
        super();
    }

    public DynamicInstanceFieldException(String message) {
        super(message);
    }

    public DynamicInstanceFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public DynamicInstanceFieldException(Throwable cause) {
        super(cause);
    }

}
