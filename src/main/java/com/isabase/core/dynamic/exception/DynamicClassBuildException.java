package com.isabase.core.dynamic.exception;

/**
 * Created by otavio on 24/12/2015.
 */
public class DynamicClassBuildException extends Exception {

    public DynamicClassBuildException() {
        super();
    }

    public DynamicClassBuildException(String message) {
        super(message);
    }

    public DynamicClassBuildException(String message, Throwable cause) {
        super(message, cause);
    }

    public DynamicClassBuildException(Throwable cause) {
        super(cause);
    }

}
