package com.isabase.core.dynamic;

/**
 * Created by otavio on 20/11/2015.
 */
public class DynamicConstant {

    public static final String DYNAMIC_PACKAGE = "com.isabase.entity.dynamic";

    public static final String DYNAMIC_CLASSES_LOADED_LOG = "Dynamic entity classes loaded in {0}ms";

    public static final String DYNAMIC_CLASS_LOAD_LOG = "Loading dynamic entity class: {0}";

    public static final String INSTANCE_FIELD_ACCESS_EXCEPTION = "Error accessing dynamic instance field.";

    public static final String DYNAMIC_CLASS_BUILD_EXCEPTION = "Error on build dynamic entity class {0}.";
}