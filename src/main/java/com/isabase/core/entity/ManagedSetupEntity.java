package com.isabase.core.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;

/**
 * Created by otavio on 11/01/2016.
 */
@Data
@Entity
@RequiredArgsConstructor
@NoArgsConstructor
public class ManagedSetupEntity extends BaseEntity<Integer> {

    @NonNull
    private String name;

    private boolean executed;

}
