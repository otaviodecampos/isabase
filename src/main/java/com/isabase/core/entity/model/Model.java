package com.isabase.core.entity.model;

import com.isabase.core.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Created by otavio on 21/11/2015.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Model extends BaseEntity<Integer> {
}
