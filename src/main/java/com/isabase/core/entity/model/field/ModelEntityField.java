package com.isabase.core.entity.model.field;

import com.isabase.core.entity.model.EntityField;
import com.isabase.core.entity.model.Model;
import com.isabase.util.StringUtil;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by otavio on 29/11/2015.
 */
@Data
@NoArgsConstructor
@JsonTypeName("model")
public class ModelEntityField extends EntityField<Model> {

    private String target;

    private boolean collection = false;

    public ModelEntityField(String name) {
        super(name);
    }

    public String getTarget() {
        return StringUtil.capitalize(target);
    }

}
