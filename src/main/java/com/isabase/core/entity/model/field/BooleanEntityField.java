package com.isabase.core.entity.model.field;

import com.isabase.core.entity.model.EntityField;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by otavio on 29/11/2015.
 */
@Data
@NoArgsConstructor
@JsonTypeName("boolean")
public class BooleanEntityField extends EntityField<Boolean> {

    public BooleanEntityField(String name) {
        super(name);
    }

}
