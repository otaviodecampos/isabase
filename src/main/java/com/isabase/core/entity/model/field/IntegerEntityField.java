package com.isabase.core.entity.model.field;

import com.isabase.core.entity.model.EntityField;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by otavio on 29/11/2015.
 */
@Data
@NoArgsConstructor
@JsonTypeName("integer")
public class IntegerEntityField extends EntityField<Integer> {

    public IntegerEntityField(String name) {
        super(name);
    }

}
