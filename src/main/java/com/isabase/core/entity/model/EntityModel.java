package com.isabase.core.entity.model;

import com.isabase.core.persistence.converter.EntityFieldConverter;
import com.isabase.core.persistence.listener.EntityModelListener;
import com.isabase.util.StringUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 20/11/2015.
 */
@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@EntityListeners(EntityModelListener.class)
public class EntityModel extends Model {

    @NonNull
    @Column(unique = true)
    private String name;

    @OneToOne
    private Model parent;

    @Convert(converter = EntityFieldConverter.class)
    private List<EntityField> fields = new ArrayList<>();

    @JsonIgnore
    private String owner;

    public String getName() {
        return StringUtil.capitalize(name);
    }

    public EntityField getField(String name) {
        for (EntityField field : fields) {
            if (field.getName().equals(name)) {
                return field;
            }
        }
        return null;
    }

    public void addField(EntityField field) {
        getFields().add(field);
    }

}
