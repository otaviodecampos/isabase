package com.isabase.core.entity.model;

import com.isabase.core.entity.model.field.*;

import java.lang.reflect.ParameterizedType;

/**
 * Created by otavio on 29/11/2015.
 */
public enum FieldType {

    Text(TextEntityField.class),
    Integer(IntegerEntityField.class),
    Decimal(DecimalEntityField.class),
    Date(DateEntityField.class),
    Boolean(BooleanEntityField.class),
    Model(ModelEntityField.class);

    private Class clazz;

    private Class classType;

    FieldType(Class<? extends EntityField> clazz) {
        this.clazz = clazz;
        this.classType = (Class) ((ParameterizedType) clazz
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public static EntityField newInstanceByClass(Class clazz) {
        EntityField instance = null;
        for (FieldType fieldType : FieldType.values()) {
            if (fieldType.getClassType() == clazz) {
                try {
                    instance = (EntityField) fieldType.getClazz().newInstance();
                    break;
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return instance;
    }

    public Class getClazz() {
        return clazz;
    }

    private Class getClassType() {
        return classType;
    }
}
