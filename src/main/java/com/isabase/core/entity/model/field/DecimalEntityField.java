package com.isabase.core.entity.model.field;

import com.isabase.core.entity.model.EntityField;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * Created by otavio on 29/11/2015.
 */
@Data
@NoArgsConstructor
@JsonTypeName("decimal")
public class DecimalEntityField extends EntityField<BigDecimal> {

    private Integer precision;

    private Integer scale;

    public DecimalEntityField(String name) {
        super(name);
    }
}
