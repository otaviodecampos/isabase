package com.isabase.core.entity.model.field;

import com.isabase.core.entity.model.EntityField;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by otavio on 29/11/2015.
 */
@Data
@NoArgsConstructor
@JsonTypeName("date")
public class DateEntityField extends EntityField<Date> {

    public DateEntityField(String name) {
        super(name);
    }

}
