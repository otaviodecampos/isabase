package com.isabase.util;

/**
 * Created by otavio on 16/01/2016.
 */
public class ClassUtil {

    private static final String REPLACE_PATTERN = "(\\$.+)";

    public static String getName(Class clazz) {
        return clazz.getSimpleName().replaceAll(REPLACE_PATTERN, "");
    }

    public static String getPath(Class clazz) {
        return clazz.getCanonicalName().replaceAll(REPLACE_PATTERN, "");
    }

}
