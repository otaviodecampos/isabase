package com.isabase.util;

/**
 * Created by otavio on 20/11/2015.
 */
public class StringUtil {

    public static String capitalize(String str) {
        if (str != null && str.length() >= 2) {
            return Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
        return null;
    }

}
