package com.isabase.util;

import static spark.Spark.after;

/**
 * Created by otavio on 20/11/2015.
 */
public class RouteUtil {

    public static void setResponseJson(String path) {
        after(path, (req, res) -> {
            res.type("application/json");
        });
    }

}
