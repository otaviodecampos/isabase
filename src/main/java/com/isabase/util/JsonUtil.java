package com.isabase.util;

import com.isabase.core.dynamic.util.DynamicUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectWriter;
import spark.ResponseTransformer;

import java.io.IOException;

/**
 * Created by otavio on 14/11/2015.
 */
public class JsonUtil {

    public static String toJson(Object object) throws JsonProcessingException {
        return toJson(object, null);
    }

    public static String toJson(Object object, TypeReference<?> rootType) throws JsonProcessingException {
        ObjectWriter ow = DynamicUtil.newObjectMapper().writerFor(rootType).withDefaultPrettyPrinter();
        return ow.writeValueAsString(object);
    }

    public static ResponseTransformer json() {
        return JsonUtil::toJson;
    }

    public static <T> T fromJson(String json, Class clazz) throws IOException {
        return (T) DynamicUtil.newObjectMapper().readValue(json, clazz);
    }

    public static <T> T fromJson(String json, TypeReference typeReference) throws IOException {
        return (T) DynamicUtil.newObjectMapper().readValue(json, typeReference);
    }

}
