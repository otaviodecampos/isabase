package com.isabase.repository;

import com.isabase.core.entity.ManagedSetupEntity;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;

/**
 * Created by otavio on 21/11/2015.
 */
@Repository
public interface ManagedSetupRepository extends EntityRepository<ManagedSetupEntity, Integer> {

    @Query(singleResult = SingleResultType.OPTIONAL)
    ManagedSetupEntity findByNameEqual(String name);

}
