package com.isabase.repository;

import java.util.List;

/**
 * Created by otavio on 27/12/2015.
 */
public interface Repository<T, PK> {

    public List<T> findAll(String name) throws Exception;

    T findById(String name, PK pk) throws Exception;

    T save(T instance);

    void remove(T instance);

}
