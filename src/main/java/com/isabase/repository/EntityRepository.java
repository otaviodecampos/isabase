package com.isabase.repository;

import com.isabase.core.dynamic.DynamicManager;
import com.isabase.core.entity.BaseEntity;
import com.isabase.core.entity.model.EntityModel;
import com.isabase.core.entity.model.QEntityModel;
import com.isabase.security.CurrentUser;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by otavio on 21/11/2015.
 */
public class EntityRepository implements Repository<BaseEntity, Integer> {

    @Inject
    private EntityManager em;

    @Inject
    private DynamicManager dm;

    @Inject
    private CurrentUser user;

    @Override
    public List<BaseEntity> findAll(String name) throws ClassNotFoundException {
        JPAQuery q = new JPAQuery(em);
        Class clazz = dm.getEntityClass(name);
        PathBuilder<BaseEntity> path = new PathBuilder(clazz, "e");
        q = q.from(path);

        if(clazz.equals(EntityModel.class)) {
            q.where(path.get(QEntityModel.entityModel.owner).eq(user.getLoginName()));
        }

        return q.list(path);
    }

    public BaseEntity findById(String name, Integer pk) throws ClassNotFoundException {
        return findUniqueByField(name, "id", pk);
    }

    public <T> BaseEntity findUniqueByField(String name, String fieldName, T fieldValue) throws ClassNotFoundException {
        JPAQuery q = new JPAQuery(em);
        Class clazz = dm.getEntityClass(name);
        PathBuilder<BaseEntity> path = new PathBuilder(clazz, "e");
        q = q.from(path).where(path.get(fieldName).eq(fieldValue));

        if(clazz.equals(EntityModel.class)) {
            q.where(path.get(QEntityModel.entityModel.owner).eq(user.getLoginName()));
        }

        return q.uniqueResult(path);
    }

    @Transactional
    public BaseEntity save(BaseEntity instance) {
        if (instance.isNew()) {
            em.persist(instance);
        } else {
            instance = em.merge(instance);
        }

        return instance;
    }

    @Transactional
    public void remove(BaseEntity instance) {
        em.remove(instance);
    }

}