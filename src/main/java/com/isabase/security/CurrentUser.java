package com.isabase.security;

import org.picketlink.Identity;
import org.picketlink.authentication.AuthenticationException;
import org.picketlink.authentication.levels.Level;
import org.picketlink.idm.model.basic.User;

import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by otavio on 19/01/2016.
 */
public class CurrentUser {

    @Inject
    private Identity identity;

    private CurrentUser() {}

    public boolean isLoggedIn() {
        return identity.isLoggedIn();
    }

    public Level getLevel() {
        return identity.getLevel();
    }

    public User getAccount() {
        return (User) identity.getAccount();
    }

    public Identity.AuthenticationResult login() throws AuthenticationException {
        return identity.login();
    }

    public void logout() {
        identity.logout();
    }

    public boolean hasPermission(Object o, String s) {
        return identity.hasPermission(o, s);
    }

    public boolean hasPermission(Class<?> aClass, Serializable serializable, String s) {
        return identity.hasPermission(aClass, serializable, s);
    }

    public String getLoginName() {
        if(isLoggedIn()) {
            return getAccount().getLoginName();
        }
        return null;
    }

}
