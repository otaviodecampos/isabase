package com.isabase.security;

import org.picketlink.idm.config.IdentityConfiguration;
import org.picketlink.idm.config.IdentityConfigurationBuilder;

import javax.enterprise.inject.Produces;

/**
 * Created by otavio on 16/01/2016.
 */
public class IdentityConfigurationInitializer {

    @Produces
    IdentityConfiguration init() {
        IdentityConfigurationBuilder builder = new IdentityConfigurationBuilder();
        
        builder.named("default")
                .stores()
                .jpa()
                .supportAllFeatures();

        return builder.build();
    }

}
