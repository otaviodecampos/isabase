package com.isabase.security;

import org.picketlink.config.SecurityConfigurationBuilder;
import org.picketlink.event.SecurityConfigurationEvent;

import javax.enterprise.event.Observes;

/**
 * Created by otavio on 16/01/2016.
 */
public class SecurityConfigurationInitializer {

    public void init(@Observes SecurityConfigurationEvent event) {
        SecurityConfigurationBuilder builder = event.getBuilder();
        builder.identity().stateless();
    }

}
