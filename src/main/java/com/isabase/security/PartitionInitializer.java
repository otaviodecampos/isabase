package com.isabase.security;

import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.picketlink.annotations.PicketLink;
import org.picketlink.event.PartitionManagerCreateEvent;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.config.SecurityConfigurationException;
import org.picketlink.idm.model.basic.Realm;

import javax.enterprise.event.Observes;

/**
 * Created by otavio on 16/01/2016.
 */
@Transactional(qualifier = PicketLink.class)
public class PartitionInitializer {

    public void init(@Observes PartitionManagerCreateEvent event) {
        createDefaultPartition(event.getPartitionManager());
    }

    private void createDefaultPartition(PartitionManager partitionManager) {
        Realm partition = partitionManager.getPartition(Realm.class, Realm.DEFAULT_REALM);

        if (partition == null) {
            try {
                partition = new Realm(Realm.DEFAULT_REALM);
                partitionManager.add(partition);
            } catch (Exception e) {
                throw new SecurityConfigurationException("Could not create default partition.", e);
            }
        }
    }

}
