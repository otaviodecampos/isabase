package com.isabase.filter;

import com.isabase.security.CurrentUser;
import org.picketlink.Identity;
import org.picketlink.credential.DefaultLoginCredentials;
import spark.Request;
import spark.Response;

import javax.inject.Inject;
import java.util.Base64;

import static spark.Spark.halt;

/**
 * Created by otavio on 09/01/2016.
 */
public class SecurityFilter implements Filter {

    @Inject
    private DefaultLoginCredentials loginCredentials;

    @Inject
    private CurrentUser user;

    @Override
    public void before(Request request, Response response) {
        if(!user.isLoggedIn()) {
            loginByBasicAuth(request);
            if (!user.login().equals(Identity.AuthenticationResult.SUCCESS)) {
                response.header("WWW-Authenticate", "Basic realm=\"Restricted\"");
                halt(401);
            }
        }
    }

    @Override
    public void after(Request request, Response response) {

    }

    private void loginByBasicAuth(Request request) {
        String auth = request.headers("Authorization");
        if(auth != null && auth.startsWith("Basic")) {
            String b64Credentials = auth.substring("Basic".length()).trim();
            String[] credentials = new String(Base64.getDecoder().decode(b64Credentials)).split(":");
            loginCredentials.setUserId(credentials[0]);
            loginCredentials.setPassword(credentials[1]);
        }
    }

}