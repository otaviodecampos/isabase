package com.isabase.filter;

import spark.Request;
import spark.Response;

/**
 * Created by otavio on 09/01/2016.
 */
public interface Filter {

    void before(Request request, Response response);

    void after(Request request, Response response);

}
