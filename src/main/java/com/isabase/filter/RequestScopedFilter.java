package com.isabase.filter;

import org.apache.deltaspike.cdise.api.ContextControl;
import org.apache.deltaspike.core.api.provider.BeanProvider;
import spark.Request;
import spark.Response;

import javax.enterprise.context.RequestScoped;

/**
 * Created by otavio on 09/01/2016.
 */
public class RequestScopedFilter implements Filter {

    @Override
    public void before(Request request, Response response) {
        ContextControl ctxCtrl = BeanProvider.getContextualReference(ContextControl.class);
        ctxCtrl.startContext(RequestScoped.class);
    }

    @Override
    public void after(Request request, Response response) {
        ContextControl ctxCtrl = BeanProvider.getContextualReference(ContextControl.class);
        ctxCtrl.stopContext(RequestScoped.class);
    }

}
