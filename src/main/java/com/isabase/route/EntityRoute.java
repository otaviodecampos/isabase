package com.isabase.route;

import com.isabase.core.Version;
import com.isabase.core.dynamic.DynamicManager;
import com.isabase.core.entity.BaseEntity;
import com.isabase.service.EntityService;
import com.isabase.util.JsonUtil;
import spark.Spark;

import javax.inject.Inject;

import static com.isabase.util.JsonUtil.fromJson;
import static spark.Spark.*;

/**
 * Created by otavio on 21/11/2015.
 */
public class EntityRoute implements Route {

    private final String path = "/api/" + Version.version + "/entity";

    private final String listPath = path.concat("/:name");

    private final String entityPath = listPath.concat("/:id");

    @Inject
    private EntityService service;

    @Inject
    private DynamicManager dm;

    @Override
    public void findAll() {
        Spark.get(path, (req, res) -> {
           return dm.getModels();
        }, JsonUtil.json());

        Spark.get(listPath, (req, res) -> {
            return service.findAll(req.params("name"));
        }, JsonUtil.json());
    }

    @Override
    public void findById() {
        Spark.get(entityPath, (req, res) -> {
            return service.findById(req.params("name"), Integer.parseInt(req.params("id")));
        }, JsonUtil.json());
    }

    @Override
    public void save() {
        Spark.post(listPath, (req, res) -> {
            BaseEntity instance = service.save(fromJson(req.body(), dm.getEntityClass(req.params("name"))));
            return instance;
        }, JsonUtil.json());
    }

    @Override
    public void remove() {
        Spark.delete(entityPath, (req, resp) -> {
            BaseEntity instance = service.findById(req.params("name"), Integer.parseInt(req.params("id")));
            service.remove(instance);
            return instance;
        }, JsonUtil.json());
    }

    @Override
    public void onException() {
        exception(ClassNotFoundException.class, (e, request, response) -> {
            response.status(404);
            response.body(e.getMessage());
        });
    }
}
