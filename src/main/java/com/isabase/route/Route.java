package com.isabase.route;

/**
 * Created by otavio on 21/11/2015.
 */
public interface Route {

    void findAll();

    void findById();

    void save();

    void remove();

    void onException();

}
