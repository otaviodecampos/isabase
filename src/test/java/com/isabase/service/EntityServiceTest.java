package com.isabase.service;

import com.isabase.core.ApplicationManager;
import com.isabase.core.dynamic.DynamicEntity;
import com.isabase.core.dynamic.DynamicEntityField;
import com.isabase.core.dynamic.DynamicManager;
import com.isabase.core.dynamic.exception.DynamicInstanceFieldException;
import com.isabase.core.entity.BaseEntity;
import com.isabase.core.entity.model.EntityField;
import com.isabase.core.entity.model.EntityModel;
import com.isabase.core.entity.model.field.TextEntityField;
import org.apache.deltaspike.core.api.provider.BeanProvider;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 20/12/2015.
 */
@RunWith(CdiTestRunner.class)
public class EntityServiceTest {

    private static final String ENTITY_NAME = "User";

    private static final String FIELD_NAME = "name";

    private static final String INSTANCE_NAME = "user1";

    @Inject
    private EntityService service;

    @Inject
    private DynamicManager dm;

    @BeforeClass
    public static void init() {
        ApplicationManager.runInContext(RequestScoped.class, () -> {
            EntityModel model = new EntityModel("User");

            List<EntityField> fields = new ArrayList();
            model.setFields(fields);

            TextEntityField name = new TextEntityField();
            name.setName("name");
            fields.add(name);

            DynamicManager dm1 = BeanProvider.getContextualReference(DynamicManager.class);
            EntityService service1 = BeanProvider.getContextualReference(EntityService.class, false);

            service1.save(model);
            dm1.updateDynamicClasses();
        });
    }

    @Test
    public void testSave() throws DynamicInstanceFieldException, ClassNotFoundException {
        DynamicEntity instance = (DynamicEntity) service.save(createInstance());
        DynamicEntityField<String> fieldName = instance.getField(FIELD_NAME);

        Assert.assertEquals(fieldName.getValue(), INSTANCE_NAME);
    }

    @Test
    public void testFindAll() throws DynamicInstanceFieldException, ClassNotFoundException {
        List<BaseEntity> instances = service.findAll(ENTITY_NAME);

        Assert.assertNotNull(instances);
    }

    @Test
    public void testFindById() throws DynamicInstanceFieldException, ClassNotFoundException {
        DynamicEntity instance = (DynamicEntity) service.save(createInstance());

        instance = (DynamicEntity) service.findById(ENTITY_NAME, instance.getId());
        DynamicEntity dynamicEntity = instance;
        DynamicEntityField<String> field = dynamicEntity.getField(FIELD_NAME);
        String value = field.getValue();

        Assert.assertEquals(value, INSTANCE_NAME);
    }

    @Test
    public void testRemove() throws DynamicInstanceFieldException, ClassNotFoundException {
        DynamicEntity instance = (DynamicEntity) service.save(createInstance());
        service.remove(instance);

        Assert.assertNull(service.findById(ENTITY_NAME, instance.getId()));
    }

    private DynamicEntity createInstance() throws DynamicInstanceFieldException, ClassNotFoundException {
        DynamicEntity instance = dm.newInstance(ENTITY_NAME);
        DynamicEntityField<String> name = instance.getField(FIELD_NAME);
        name.setValue(INSTANCE_NAME);
        return instance;
    }

}
