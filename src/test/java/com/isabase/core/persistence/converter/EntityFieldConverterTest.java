package com.isabase.core.persistence.converter;

import com.isabase.core.entity.model.EntityField;
import com.isabase.core.entity.model.field.*;
import com.isabase.util.FileUtil;
import com.isabase.util.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Iterables;
import org.junit.Assert;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 27/12/2015.
 */
public class EntityFieldConverterTest {

    @Test
    public void testConvertToDatabaseColumn() throws Exception {
        String json = new EntityFieldConverter().convertToDatabaseColumn(createFields());
        String expected = FileUtil.getFileAsString(this.getClass(), "expected.json");
        JSONAssert.assertEquals(expected, json, false);
    }

    @Test
    public void testConvertToEntityAttribute() throws Exception {
        String json = FileUtil.getFileAsString(this.getClass(), "expected.json");
        List<EntityField> expected = JsonUtil.fromJson(json, new TypeReference<List<EntityField>>() {});
        Assert.assertTrue(Iterables.elementsEqual(expected, createFields()));
    }

    private List<EntityField> createFields() {
        List<EntityField> fields = new ArrayList<>();
        fields.add(new TextEntityField("textField"));
        fields.add(new BooleanEntityField("booleanField"));
        fields.add(new DecimalEntityField("decimalField"));
        fields.add(new DateEntityField("dateField"));
        fields.add(new IntegerEntityField("integerField"));
        fields.add(new ModelEntityField("modelField"));
        return fields;
    }
}