package com.isabase.core.dynamic.builder.groovy.annotationfield;

import org.junit.Assert;
import org.junit.Test;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;

/**
 * Created by otavio on 27/12/2015.
 */
public class OneToManyAnnotationFieldTest {

    @Test
    public void testWrite() throws Exception {
        OneToManyAnnotationField annotation = new OneToManyAnnotationField();
        annotation.setCascade(CascadeType.ALL);
        annotation.setFetch(FetchType.EAGER);

        String expected = "@OneToMany(cascade=javax.persistence.CascadeType.ALL, fetch=javax.persistence.FetchType.EAGER)";
        Assert.assertEquals(expected, annotation.write());
    }
}