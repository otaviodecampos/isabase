package com.isabase.core.dynamic.builder.groovy.annotationfield;

import org.junit.Assert;
import org.junit.Test;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;

/**
 * Created by otavio on 27/12/2015.
 */
public class OneToOneAnnotationFieldTest {

    @Test
    public void testWrite() throws Exception {
        OneToOneAnnotationField annotation = new OneToOneAnnotationField();
        annotation.setCascade(CascadeType.ALL);
        annotation.setFetch(FetchType.EAGER);

        String expected = "@OneToOne(cascade=javax.persistence.CascadeType.ALL, fetch=javax.persistence.FetchType.EAGER)";
        Assert.assertEquals(expected, annotation.write());
    }
}