package com.isabase.core.dynamic.builder.groovy;

import com.isabase.core.entity.model.EntityModel;
import com.isabase.core.entity.model.field.TextEntityField;
import com.isabase.util.StringUtil;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by otavio on 27/12/2015.
 */
public class GrovvyClassBuilderTest {

    private static final String ENTITY_NAME = "User";

    private static final String FIELD_NAME = "name";

    @Test
    public void testBuild() throws Exception {
        EntityModel model = new EntityModel("User");
        model.addField(new TextEntityField("name"));

        Class clazz = new GrovvyClassBuilder().build(model);
        Assert.assertEquals(clazz.getSimpleName(), ENTITY_NAME);

        Field fieldName = clazz.getDeclaredField(FIELD_NAME);
        Assert.assertEquals(fieldName.getName(), FIELD_NAME);
        Assert.assertEquals(fieldName.getType(), String.class);

        Method setter = clazz.getDeclaredMethod("set" + StringUtil.capitalize(FIELD_NAME), model.getField(FIELD_NAME).getClassType());
        Method getter = clazz.getDeclaredMethod("get" + StringUtil.capitalize(FIELD_NAME));

        Assert.assertNotNull(setter);
        Assert.assertNotNull(getter);
    }
}