package com.isabase.core.dynamic.builder.groovy;

import com.isabase.core.dynamic.builder.groovy.annotationfield.ColumnAnnotationField;
import com.isabase.core.dynamic.builder.groovy.annotationfield.GroovyAnnotationField;
import com.isabase.core.entity.model.field.DecimalEntityField;
import com.isabase.core.entity.model.field.TextEntityField;
import com.google.common.collect.Iterables;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 27/12/2015.
 */
public class GroovyClassUtilTest {

    @Test
    public void testGetTextEntityFieldAnnotations() throws Exception {
        TextEntityField fieldText = new TextEntityField();
        fieldText.setTextarea(true);

        List<GroovyAnnotationField> expected = new ArrayList();
        ColumnAnnotationField annotation = new ColumnAnnotationField();
        annotation.setLength(2147483647);
        expected.add(annotation);

        Assert.assertTrue(Iterables.elementsEqual(expected, GroovyClassUtil.getEntityFieldAnnotations(fieldText)));
    }

    @Test
    public void testGetDecimalEntityFieldAnnotations() throws Exception {
        DecimalEntityField fieldDecimal = new DecimalEntityField();
        fieldDecimal.setPrecision(2);
        fieldDecimal.setScale(2);

        List<GroovyAnnotationField> expected = new ArrayList();
        ColumnAnnotationField annotation = new ColumnAnnotationField();
        annotation.setPrecision(2);
        annotation.setScale(2);
        expected.add(annotation);

        Assert.assertTrue(Iterables.elementsEqual(expected, GroovyClassUtil.getEntityFieldAnnotations(fieldDecimal)));
    }

    @Test
    public void testTypeByEntityField() throws Exception {

    }
}