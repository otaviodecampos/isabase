package com.isabase.core.dynamic.builder.groovy.annotationfield;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by otavio on 27/12/2015.
 */
public class ColumnAnnotationFieldTest {

    @Test
    public void testWrite() throws Exception {
        ColumnAnnotationField annotation = new ColumnAnnotationField();
        annotation.setScale(2);
        annotation.setPrecision(2);
        annotation.setLength(255);

        String expected = "@Column(precision=2, length=255, scale=2)";
        Assert.assertEquals(expected, annotation.write());
    }
}