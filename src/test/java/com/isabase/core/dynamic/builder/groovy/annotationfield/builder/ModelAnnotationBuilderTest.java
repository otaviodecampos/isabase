package com.isabase.core.dynamic.builder.groovy.annotationfield.builder;

import com.isabase.core.dynamic.builder.groovy.annotationfield.ModelAnnotationField;
import com.isabase.core.dynamic.builder.groovy.annotationfield.OneToManyAnnotationField;
import com.isabase.core.dynamic.builder.groovy.annotationfield.OneToOneAnnotationField;
import com.isabase.core.entity.model.field.ModelEntityField;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by otavio on 27/12/2015.
 */
public class ModelAnnotationBuilderTest {

    @Test
    public void testBuild() throws Exception {
        ModelEntityField fieldModel = new ModelEntityField();

        ModelAnnotationField annotation = new ModelAnnotationBuilder().build(fieldModel);
        Assert.assertTrue(annotation instanceof OneToOneAnnotationField);
    }

    @Test
    public void testBuildCollection() throws Exception {
        ModelEntityField fieldModel = new ModelEntityField();
        fieldModel.setCollection(true);

        ModelAnnotationField annotation = new ModelAnnotationBuilder().build(fieldModel);
        Assert.assertTrue(annotation instanceof OneToManyAnnotationField);
    }
}