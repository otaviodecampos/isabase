package com.isabase.core.dynamic.builder.groovy.annotationfield.builder;

import com.isabase.core.dynamic.builder.groovy.annotationfield.ColumnAnnotationField;
import com.isabase.core.entity.model.field.TextEntityField;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by otavio on 27/12/2015.
 */
public class TextAnnotationBuilderTest {

    @Test
    public void testBuild() throws Exception {
        TextEntityField fieldText = new TextEntityField();
        fieldText.setTextarea(true);

        ColumnAnnotationField annotation = new TextAnnotationBuilder().build(fieldText);
        Assert.assertEquals(annotation.getLength().intValue(), 2147483647);
    }
}