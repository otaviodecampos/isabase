package com.isabase.core.dynamic.builder.groovy.annotationfield.builder;

import com.isabase.core.dynamic.builder.groovy.annotationfield.ColumnAnnotationField;
import com.isabase.core.entity.model.field.DecimalEntityField;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by otavio on 27/12/2015.
 */
public class DecimalAnnotationBuilderTest {

    @Test
    public void testBuild() throws Exception {
        DecimalEntityField fieldDecimal = new DecimalEntityField();
        fieldDecimal.setPrecision(2);
        fieldDecimal.setScale(2);

        ColumnAnnotationField annotation = new DecimalAnnotationBuilder().build(fieldDecimal);
        Assert.assertEquals(annotation.getPrecision().intValue(), 2);
        Assert.assertEquals(annotation.getScale().intValue(), 2);
    }
}